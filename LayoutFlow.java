package awt3;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;

public class LayoutFlow {
	
	String[] mahasiswa = {"Ramadhan Wahyu", "Muhammad Salman", "One Octadion", "Prayoga Yudhitama"};
	JLabel[] mahasiswaLabel = new JLabel[mahasiswa.length]; 
	
	LayoutFlow(){
		JFrame frame = new JFrame("Presensi Mahasiswa");
		JButton button,button1, button2;
		button = new JButton("Tambah Mahasiswa");
		button1 = new JButton("Edit Mahasiswa");
		button2 = new JButton("Hapus Mahasiswa");
		
		frame.add(button, BorderLayout.NORTH);
		frame.add(button1, BorderLayout.NORTH);
		frame.add(button2, BorderLayout.NORTH);
		
		frame.add(new JList(mahasiswa));
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
		
		frame.setLayout(new FlowLayout());
		frame.setSize(300,300);  
		frame.setVisible(true);
		
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new FormAWT();
				System.out.println("click");
			}
		});
	}
	
	public static void main(String[] args) {
		new LayoutFlow();
	}
	
}
